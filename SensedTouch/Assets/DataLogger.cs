using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Text;
using System.IO;

    public class DataLogger : MonoBehaviour
{

    public enum Seperators {Comma, SemiColon, Dash};
    public Seperators Sep;

    public string Filename;
    private string currentSetting;

    private enum States {NotLogging, Logging, Savebuffer};

    private States curstate;

    private List<string> loggy;

    //it's static so we can call it from anywhere


    private string seppy(){
        if (Sep == Seperators.Comma){
            return ",";
        }
        else if (Sep == Seperators.SemiColon){
            return ";";
        }
        else if (Sep == Seperators.Dash){
            return "-";
        }
        else{
            throw new Exception("Not Comma, Dash or SemiColon");
        }      
    }

    public string header(){

        string heady = "UserId" + seppy() + "TrialId" + seppy() + "Timestamp" + seppy() + "Event" + seppy() + "PosX" + seppy() + "PosY" + seppy() + "PosZ";

        return heady;

    }

    public void SaveFile()
    {
        string filepath = Application.persistentDataPath + "/" + Filename + currentSetting + System.DateTime.Now.ToString("ddmmyyyy-HHmmss") + ".csv";
        Debug.Log(filepath);
        // Write each directory name to a file.
        using (StreamWriter sw = new StreamWriter(filepath))
        {
            Debug.Log("Oooweee");
            sw.WriteLine(header() + "\n");
            
            foreach (string line in loggy)
                {
                    sw.WriteLine(line);
                }
        }
        curstate = States.NotLogging;
    }

    public void StartLogging(string setting){
        currentSetting = setting;
        if (curstate == States.NotLogging){
            curstate = States.Logging;
            loggy = new List<string>();
        }

    }

    public void StopLogging(){

        if (curstate == States.Logging) {
            curstate = States.Savebuffer;
            SaveFile();
        }

    }

    public void Log(int userId, int trialId, long timestamp, string eventName, params string[] items){
        //string = Convert.ToString( 2.5000001);
        //string = x.ToString() + sep + y.ToString() + sep + z.ToString();
        if (curstate == States.Logging){

            List<string> stringy = new List<string>(){
                userId.ToString(),
                trialId.ToString(),
                timestamp.ToString(),
                eventName
            };
            stringy.AddRange(items);

            loggy.Add(
                String.Join(seppy(), stringy));

        }
        
    }

}

