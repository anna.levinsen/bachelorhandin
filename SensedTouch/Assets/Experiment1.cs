using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Experiment1 : MonoBehaviour
{
    private enum States{
        Idle,
        ExperimentRunning,
        SubExperimentRunning,
        Trial,
        Logging
    }
    private States state;

    public OVRInput.Button button1;
    public OVRInput.Button button2;
    public OVRInput.Button button3;
    public OVRInput.Button button4;
    public OVRInput.Button stick1;
    public OVRInput.Button stick2;

    private GameObject targetSphere;
    public GameObject straightStick, curvedStickSmall, curvedStickMedium, curvedStickLarge, curvedStickEkstraLarge;

    //private List<GameObject> sticks;
    
    //sticks = new List<GameObject>{straightStick, curvedStickSmall, curvedStickMedium, curvedStickLarge, curvedStickEkstraLarge};

    public GameObject sphereSt1, sphereSt2, sphereSt3, sphereSt4, sphereSt5;
    public GameObject sphereS1, sphereS2, sphereS3, sphereS4, sphereS5;
    public GameObject sphereM1, sphereM2, sphereM3, sphereM4, sphereM5;
    public GameObject sphereL1, sphereL2, sphereL3, sphereL4, sphereL5;
    public GameObject sphereXL1, sphereXL2, sphereXL3, sphereXL4, sphereXL5;


    public GameObject straightHandle1, straightHandle2, smallHandle1, smallHandle2, mediumHandle1, mediumHandle2, largeHandle1, largeHandle2, ekstraLargeHandle1, ekstraLargeHandle2;

    private int currentExp, currentStick, currentSub; 

    private int trialNumber;

    //private GameObject sphereLocation;
    
    public DataLogger dataLogger;
    public GameObject objectPlaceholder;
    //private bool currentlogging = false;

/*
    //CameraRay
    [SerializeField]
    private GameObject hitMarker;

    [SerializeField]
    private LayerMask selectLayer;
    Camera cam;
*/

    //private bool currentlogging = false;


    public void FunnelingAction(float left, float right) {
        float[] arrayR = {right};
        OVRInput.HapticsAmplitudeEnvelopeVibration vibrateR = new OVRInput.HapticsAmplitudeEnvelopeVibration();
        vibrateR.SamplesCount = 1;
        vibrateR.Samples = arrayR;
        vibrateR.Duration = 0.1f;
        OVRInput.SetControllerHapticsAmplitudeEnvelope(vibrateR, OVRInput.Controller.RTouch);
        float[] arrayL = {left};
        OVRInput.HapticsAmplitudeEnvelopeVibration vibrateL = new OVRInput.HapticsAmplitudeEnvelopeVibration();
        vibrateL.SamplesCount = 1;
        vibrateL.Samples = arrayL;
        vibrateL.Duration = 0.1f;
        OVRInput.SetControllerHapticsAmplitudeEnvelope(vibrateL, OVRInput.Controller.LTouch);
        //OVRInput.SetControllerVibration(1, 1, OVRInput.Controller.RTouch);
    }

    /*public void SendHapticsNonCRI(){
        OVRInput.SetControllerVibration(1, 1, OVRInput.Controller.RTouch);
    }

    public void SendHapticsCRI(){
        OVRInput.SetControllerVibration(1, 1, OVRInput.Controller.RTouch);
        OVRInput.SetControllerVibration(1, 1, OVRInput.Controller.LTouch);
    }*/


    private System.Random rng = new System.Random();  

    private List<GameObject> gameObjects;

    private List<GameObject> sticklist1, sticklist2, sticklist3, sticklist4, sticklist5/*, handleList*/; 

    private List<int> stickList;

    private List<int> experiments;

    private List<int> subexperiments;

    IDictionary<int, (float AmpL, float AmpR)> hapticIntensity = new Dictionary<int, (float AmpL, float AmpR)>(){
        {0, (1.0f, 0.0f)},
        {1, (0.75f, 0.25f)},
        {2, (0.5f, 0.5f)},
        {3, (0.25f, 0.75f)},
        {4, (0.0f, 1.0f)}
    };

    /*IDictionary<GameObject, (GameObject LeftHandle, GameObject RightHandle)> handleList = new Dictionary<GameObject, (GameObject LeftHandle, GameObject RightHandle)>(){
        {straightStick, (straightHandle1, straightHandle2)},
        {curvedStickSmall, (smallHandle1, smallHandle2)},
        {curvedStickMedium, (0.5f, 0.5f)},
        {curvedStickLarge, (0.25f, 0.75f)},
        {curvedStickEkstraLarge, (0.0f, 1.0f)}
    };*/

    private List<int> points;

    private void Shuffle<T>(IList<T> list)  
    {  
        int n = list.Count;  
        while (n > 1) {  
            n--;  
            int k = rng.Next(n + 1);  
            T value = list[k];  
            list[k] = list[n];  
            list[n] = value;  
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        dataLogger.StartLogging("FunnelingActionExperiment");
        state = States.Idle;
        /*dataLogger.StartLogging("Oooweee");
        dataLogger.Log(0, 0, 100000, "boom", "ayo");
        dataLogger.StopLogging();*/

        //CameraRay
        //hitMarker = Instantiate(hitMarker/*, hit.point, Quaternion.identity*/);
        //cam = GetComponent<Camera>();*/

        experiments = new List<int>{1,2,3,4,5};
        //stickList = new List<int>{1,2,3,4,5};

        gameObjects = new List<GameObject>{straightStick, curvedStickSmall, curvedStickMedium, curvedStickLarge, curvedStickEkstraLarge, sphereSt1, sphereSt2, sphereSt3, sphereSt4, sphereSt5, sphereS1, sphereS2, sphereS3, sphereS4, sphereS5, sphereM1, sphereM2, sphereM3, sphereM4, sphereM5, sphereL1, sphereL2, sphereL3, sphereL4, sphereL5, sphereXL1, sphereXL2, sphereXL3, sphereXL4, sphereXL5, objectPlaceholder, straightHandle1, straightHandle2, smallHandle1, smallHandle2, mediumHandle1, mediumHandle2, largeHandle1, largeHandle2, ekstraLargeHandle1, ekstraLargeHandle2/*, straightHandles, smallHandles, mediumHandles, largeHandles, ekstraLargeHandles*/};

        sticklist1 = new List<GameObject>{sphereSt1, sphereSt2, sphereSt3, sphereSt4, sphereSt5};
        sticklist2 = new List<GameObject>{sphereS1, sphereS2, sphereS3, sphereS4, sphereS5};
        sticklist3 = new List<GameObject>{sphereM1, sphereM2, sphereM3, sphereM4, sphereM5};
        sticklist4 = new List<GameObject>{sphereL1, sphereL2, sphereL3, sphereL4, sphereL5};
        sticklist5 = new List<GameObject>{sphereXL1, sphereXL2, sphereXL3, sphereXL4, sphereXL5};

        //handleList = new List<GameObject>{straightHandles, smallHandles, mediumHandles, largeHandles, ekstraLargeHandles};

        Shuffle(experiments);
        //Shuffle(stickList);

        HideObjects();

        //straightStick.SetActive(true);

        //StartCoroutine(ShowAndHide(3.0f));
        /*for (int i = 0; i < length; i++)
        {
            spherelist[0].SetActive(true);
            StartCoroutine(WaitTime(3.0f));
        } */ 
    }

    // Update is called once per frame
    void Update()
    {
        /*if ((OVRInput.GetDown(button1) && OVRInput.Get(button2)) || (OVRInput.Get(button1) && OVRInput.GetDown(button2))) {
            Debug.Log("A and B button pressed");
            if (stickList.Count > 0){
                currentStick = stickList[0];

                Debug.Log($"Experiment with stick: {currentStick} has started");

                randomStick(currentStick);
                stickList.RemoveAt(0);
            }
        }*/
        if (((OVRInput.GetDown(button3) && OVRInput.Get(button4)) || (OVRInput.Get(button3) && OVRInput.GetDown(button4))) && state == States.Idle){
            Debug.Log("X and Y button pressed");
            if (experiments.Count > 0){
                state = States.ExperimentRunning;

                currentExp = experiments[0];

                subexperiments = new List<int>{1,2,3};
                Shuffle(subexperiments);

                Debug.Log($"Experiment {currentExp} has started");

                HideObjects();

                //handleList[currentExp].SetActive(true);

                //Debug.Log($"Handles {handleList[currentExp]} has been spawned");

                ExperimentManager(currentExp, currentSub);
                experiments.RemoveAt(0);
            }
            else{
                dataLogger.StopLogging();
                Debug.Log("Stop logging");
                state = States.Idle;
            }
        }

        if (((OVRInput.GetDown(button2) && OVRInput.Get(button4)) || (OVRInput.Get(button2) && OVRInput.GetDown(button4))) && state == States.ExperimentRunning) {
            Debug.Log("Y and B button pressed");
            if (subexperiments.Count > 0){
                state = States.SubExperimentRunning;

                points = new List<int>{0,0,0,0,0,0,1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3,3,3,4,4,4,4,4,4};
                //points = new List<int>{0, 1, 2, 3, 4}; //Temporary

                Shuffle(points);

                Debug.Log($"liste af spheres : {points}");

                currentSub = subexperiments[0];
            
                Debug.Log($"Experiment {currentExp} subexperiment number {currentSub} has been chosen");

                ExperimentManager(currentExp, currentSub);

                subexperiments.RemoveAt(0);
            }
            else{
                state = States.Idle;
            }
            
        }
        if (OVRInput.GetDown(button1) && state == States.Logging){
            /*Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, selectLayer)){
                hitMarker.transform.position = hit.point;
                //hitMarker.SetActive(true);
                Debug.Log($"I am looking at {hit.transform.name}");
            }
            else{
                Debug.Log("I am looking at nothing");
            }*/
            /*if (!currentlogging)
            {
                currentlogging = true;
                dataLogger.StartLogging("");

            }*/
            dataLogger.Log(
            1, 
            trialNumber, 
            System.DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(),
            "BachelorProject",
            objectPlaceholder.transform.position.x.ToString(),
            objectPlaceholder.transform.position.y.ToString(),
            objectPlaceholder.transform.position.z.ToString(), currentExp.ToString(), currentSub.ToString(), targetSphere.ToString(), targetSphere.transform.position.x.ToString(), targetSphere.transform.position.y.ToString(), targetSphere.transform.position.z.ToString());

            objectPlaceholder.SetActive(false);

            // LOG LINE HERE
            state = States.Trial;
            ExperimentManager(currentExp, currentSub);
            
        }
        /*if (OVRInput.GetDown(button1)) {
            //Debug.Log("test, is the scene transferred to the headset?");
            Debug.Log("A button pressed before");
            SendHapticsCRI(); //Oculus
            //FunnelingAction();
            Debug.Log("A button pressed after");
        }*/
        if ((OVRInput.Get(stick1) && OVRInput.GetDown(stick2)) || (OVRInput.GetDown(stick1) && OVRInput.Get(stick2))){
            Debug.Log("sticks are down");
            SceneManager.LoadScene("BachelorHandGrab");
        }
    }

    private void HideObjects(){

        foreach (GameObject gameObj in gameObjects)
        {
            gameObj.SetActive(false);
        }
    }

    private IEnumerator ShowAndHide(float delay, GameObject sphere)
    {
        sphere.SetActive(true);
        //FunnelingAction(1.0f, 1.0f);
        yield return new WaitForSeconds(delay);
        sphere.SetActive(false);  

        /*sticklist1 = new List<GameObject>{sphereSt1, sphereSt2, sphereSt3, sphereSt4, sphereSt5};
        for (int i = 0; i < 5; i++)
        {
            sticklist1[i].SetActive(true);
            yield return new WaitForSeconds(delay);
            sticklist1[i].SetActive(false);
        }*/
    }

    /*private IEnumerator WaitTime(float visualtime)
    {
        yield return new WaitForSeconds(visualtime);
    }*/

    private void HandleSpawner(int sticknumber){
        switch (sticknumber){
            case 1:
                straightHandle1.SetActive(true);
                straightHandle2.SetActive(true);
                break;
            case 2:
                smallHandle1.SetActive(true);
                smallHandle2.SetActive(true);
                break; 
            case 3:
                mediumHandle1.SetActive(true);
                mediumHandle2.SetActive(true);
                break; 
            case 4:
                largeHandle1.SetActive(true);
                largeHandle2.SetActive(true);
                break; 
            case 5:
                ekstraLargeHandle1.SetActive(true);
                ekstraLargeHandle2.SetActive(true);
                break;                
        }
    }

    private void ExperimentManager(int expNumber, int subNumber){
        HandleSpawner(expNumber);
        switch (expNumber) {
            case 1:
                Experiment(straightStick, sticklist1, subNumber);
                break;
            case 2:
                Experiment(curvedStickSmall, sticklist2, subNumber);
                break;
            case 3:
                Experiment(curvedStickMedium, sticklist3, subNumber);
                break;
            case 4:
                Experiment(curvedStickLarge, sticklist4, subNumber);
                break;
            case 5:
                Experiment(curvedStickEkstraLarge, sticklist5, subNumber);
                break;                
        }

    }

    public void Experiment(GameObject stick, List<GameObject> spheres, int subNumber){
        //stick.SetActive(true); // Should be switched to the handles of the stick.
        if (state == States.SubExperimentRunning || state == States.Trial) {
            switch (subNumber) {
                case 0:
                    break;
                case 1:
                    StartCoroutine(Subexperiment1(stick, spheres));
                    break;
                case 2:
                    StartCoroutine(Subexperiment2(stick, spheres));
                    break;
                case 3:
                    StartCoroutine(Subexperiment3(stick, spheres));
                    break;
            }
        }
    }

    public IEnumerator Subexperiment1(GameObject stick, List<GameObject> spheres){
        stick.SetActive(true);
        int pointIndex;
        if (points.Count > 0){
            trialNumber++;
            yield return new WaitForSeconds(2.0f);
            pointIndex = points[0];
            GameObject sphere = spheres[pointIndex];
            targetSphere = sphere;
            Debug.Log($"sphere, {sphere} has been chosen");
            //(float,float) intensityR, intensityL = hapticIntensity[blockNumber];
            float intensityR = hapticIntensity[pointIndex].AmpR;
            float intensityL = hapticIntensity[pointIndex].AmpL;
            //StartCoroutine(ShowAndHide(0.1f, sphere));
            FunnelingAction(intensityL, intensityR);
            Debug.Log($"pointIndex {pointIndex} has been chosen");
            points.RemoveAt(0);
            objectPlaceholder.SetActive(true);
            state = States.Logging;
            }
        else{
            trialNumber = 0;            
            state = States.ExperimentRunning;
        }
        
    }

    public IEnumerator Subexperiment2(GameObject stick, List<GameObject> spheres){
        stick.SetActive(false);
        //state = States.Idle;
        int pointIndex;
        if (points.Count > 0){
            trialNumber++;
            yield return new WaitForSeconds(2.0f);
            pointIndex = points[0];
            GameObject sphere = spheres[pointIndex];
            targetSphere = sphere;
            Debug.Log($"sphere, {sphere} has been chosen");
            //(float,float) intensityR, intensityL = hapticIntensity[blockNumber];
            float intensityR = hapticIntensity[pointIndex].AmpR;
            float intensityL = hapticIntensity[pointIndex].AmpL;
            StartCoroutine(ShowAndHide(0.1f, sphere));
            FunnelingAction(intensityL, intensityR);
            Debug.Log($"pointIndex {pointIndex} has been chosen");
            points.RemoveAt(0);
            objectPlaceholder.SetActive(true);
            state = States.Logging;
        }
        else{
            trialNumber = 0;            
            state = States.ExperimentRunning;
        }
    }

    public IEnumerator Subexperiment3(GameObject stick, List<GameObject> spheres){
        //state = States.Idle;
        stick.SetActive(true);
        int pointIndex;
        if (points.Count > 0){
            trialNumber++;
            yield return new WaitForSeconds(2.0f);
            pointIndex = points[0];
            GameObject sphere = spheres[pointIndex];
            targetSphere = sphere;
            Debug.Log($"sphere, {sphere} has been chosen");
            //(float,float) intensityR, intensityL = hapticIntensity[blockNumber];
            float intensityR = hapticIntensity[pointIndex].AmpR;
            float intensityL = hapticIntensity[pointIndex].AmpL;
            StartCoroutine(ShowAndHide(0.1f, sphere));
            FunnelingAction(intensityL, intensityR);
            Debug.Log($"pointIndex {pointIndex} has been chosen");
            points.RemoveAt(0);
            objectPlaceholder.SetActive(true);
            state = States.Logging;
        }
        else{
            trialNumber = 0;
            state = States.ExperimentRunning;
        }
    }

        /*bool isEven;
        int subNumber = 0;
        if (subexperiments.Count > 0){
                subNumber = subexperiments[0];
                if (subNumber % 2 == 0){
                    isEven = true;
                    subexperiments.Remove(2);
                    subexperiments.Remove(4);
                }
                else{
                    isEven = false;
                    subexperiments.Remove(1);
                    subexperiments.Remove(3);
                }
                Debug.Log($"Experiment 3 subexperiment number {subNumber} has been chosen");

            switch (isEven) {
                case true:
                    Debug.Log("Experiment 3 subexperiment1 has started");
                    E3Sub1();
                    break;
                case false:
                    Debug.Log("Experiment 3 subexperiment2 has started");
                    E3Sub2();
                    break;
            }
        }*/

    //GetComponent(MeshRenderer).enabled = false;
}