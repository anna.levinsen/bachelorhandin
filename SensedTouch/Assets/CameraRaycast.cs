using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycast : MonoBehaviour
{
    [SerializeField]
    private GameObject hitMarker;

    [SerializeField]
    private LayerMask selectLayer;
    Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, selectLayer)){
            hitMarker.transform.position = hit.point;
            //hitMarker.SetActive(true);
            Debug.Log($"I am looking at {hit.transform.name}");
        }
        else{
            Debug.Log("I am looking at nothing");
        }
    }
}
