using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupController : MonoBehaviour
{

    [Header("Pickup Settings")]
    [SerializeField] Transform HoldArea;
    private GameObject HeldObj;
    private Rigidbody HeldObjRB;

    [Header("Physics Parameters")]
    [SerializeField] private float pickupRange = 5.0f;
    [SerializeField] private float pickupForce = 150.0f;

    private void Update(){
        if (OVRInput.Get(OVRInput.Button.One)){
            if (HeldObj == null){
                RaycastHit hit;
                if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward),out hit, pickupRange)){
                    PickupObject(hit.transform.gameObject);
                }
            }
            else {DropObject();}
        }
        if (HeldObj != null){
            MoveObject();
        }
    }

    void MoveObject(){
        if(Vector3.Distance(HeldObj.transform.position, HoldArea.position) > 0.1f){
            Vector3 moveDirection = (HoldArea.position - HeldObj.transform.position); 
            HeldObjRB.AddForce(moveDirection * pickupForce);
        }
    }

    void PickupObject(GameObject pickObj) {
        
        if(pickObj.GetComponent<Rigidbody>()){
            HeldObjRB = pickObj.GetComponent<Rigidbody>();
            HeldObjRB.useGravity = false;
            HeldObjRB.drag = 10;
            HeldObjRB.constraints = RigidbodyConstraints.FreezeRotation;

            HeldObjRB.transform.parent = HoldArea;
            HeldObj = pickObj;
        }

    }


    void DropObject() {
        
        HeldObjRB.useGravity = true;
        HeldObjRB.drag = 1;
        HeldObjRB.constraints = RigidbodyConstraints.None;

        HeldObjRB.transform.parent = null;
        HeldObj = null;
        

    }

}
